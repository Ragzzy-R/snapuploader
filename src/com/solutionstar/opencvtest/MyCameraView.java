package com.solutionstar.opencvtest;

import org.opencv.android.JavaCameraView;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class MyCameraView extends JavaCameraView implements PictureCallback {

	public static ArrayList<Mat> imgs = new ArrayList<Mat>(10);
    private static final String TAG = "myCameraView";
    private String mPictureFileName;
    private Paint mPaint;
    private int height;
    private int width;
    public static ImageStitcher is  = new ImageStitcher();
    private boolean startCapture = false;
    private DisplayMetrics displayMetrics;
    public MyCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.GREEN);
        mPaint.setStrokeWidth(10);
        mPaint.setStyle(Paint.Style.STROKE);
        displayMetrics = CVTestActivity.metrics;
        setWillNotDraw(false);
        viewHandler.post(updateView);
        height = displayMetrics.heightPixels;
    	width = displayMetrics.widthPixels;
    //	
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedColorEffects();
    }

    public boolean isEffectSupported() {
        return (mCamera.getParameters().getColorEffect() != null);
    }

    public String getEffect() {
        return mCamera.getParameters().getColorEffect();
    }

    public void setEffect(String effect) {
        Camera.Parameters params = mCamera.getParameters();
        params.setColorEffect(effect);
        mCamera.setParameters(params);
    }

    public List<Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public void setResolution(Size resolution) {
        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());
    }

    public Size getResolution() {
        return mCamera.getParameters().getPreviewSize();
    }

    public void takePicture(final String fileName) {
        Log.i(TAG, "Taking picture");
        startCapture = true;
        this.mPictureFileName = fileName;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        //convertToMat(data);
        // Write the image in a file (in jpeg format)
        try {
            FileOutputStream fos = new FileOutputStream(mPictureFileName);
            
            fos.write(data);
            fos.close();
            Log.i(TAG, "saved file to "+ mPictureFileName);
            Toast.makeText(getContext(), "Picture saved to " + mPictureFileName, Toast.LENGTH_SHORT).show();
            MainActivity.imgspath.add(mPictureFileName);
            
        } catch (java.io.IOException e) {
            Log.e(TAG, "Exception in photoCallback", e);
        }

    }
    private void convertToMat(byte[] data) {
		
    	Bitmap bmp = BitmapFactory.decodeByteArray(data , 0, data.length);
    	Mat orig = new Mat(bmp.getHeight(),bmp.getWidth(),CvType.CV_8UC3);
    	Bitmap myBitmap32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
    	Utils.bitmapToMat(myBitmap32, orig);

    	Imgproc.cvtColor(orig, orig, Imgproc.COLOR_BGR2RGB,4);
		/*int i = is.addMatToVector(orig);
		if(i==0) {
			Toast.makeText(getContext(), "IMWRITE returned 0", Toast.LENGTH_SHORT).show();
		}
		else {
			Toast.makeText(getContext(), "IMWRITE returned"+i, Toast.LENGTH_SHORT).show();
		}*/
    	imgs.add(orig);
    	
	}

	@Override
    public void onSizeChanged(int w,int h,int oldw,int oldh) {
    	super.onSizeChanged(w, h, oldw, oldh);
    	this.invalidate();
    }
    
    /*@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
       // Try for a width based on our minimum
    	Log.i(TAG, "height = "+ height + "width = " + width);
       setMeasuredDimension(500, 500);
    }*/
    	
    @Override
    public void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
    	//x+=10;
    	if(CVTestActivity.distance < 0.002f) {
    		mPaint.setColor(Color.RED);
    	}
    	else{
    		mPaint.setColor(Color.GREEN);
    	}
    	if(startCapture) {
    	canvas.drawRect(10,10,width-(width/40),height-(height/10), mPaint);
    	Log.i ("TAG","WIDTH = " + width +"HEIGHT = "+ height);
    	}
    	//Toast.makeText(getContext(), "DRAW CALLING", Toast.LENGTH_SHORT).show();
    	Log.i(TAG, "calling Ondraw");
    
    }
    
    Handler viewHandler = new Handler();
    Runnable updateView = new Runnable(){
      @Override
      public void run(){
         invalidate();
         viewHandler.postDelayed(updateView, 1000);
      }
    };
}
