package com.solutionstar.opencvtest;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

/*
 * This is the Activity which has the camera.It takes pictures when screen is touched.
 * This activity uses CameraView of OpenCV it also uses accelerometer for guidance system.
 * Please note that the whole project needs refractoring and there are number of unused variables.
 * */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CVTestActivity extends Activity implements CvCameraViewListener2,OnTouchListener,SensorEventListener{

	  private static final String TAG = "OCVSample::Activity";
      private Mat mRgba;
      /*used for fileHandling purposes*/
      private FileUtils util;
      /*this project name is passed across activites in order to maintain the project heirarchy of the images taken*/
      private String projectName;
      private String roomName;
      private TextView mTextView;
      private CameraBridgeViewBase mOpenCvCameraView;
      private SensorManager senSensorManager;
      private Sensor senAccelerometer;
      private long lastUpdate = 0;
      private float velocity = 0;
      public static float distance = 0;
      public static DisplayMetrics metrics;
      private float last_x, last_y, last_z;
      private static final int SHAKE_THRESHOLD = 30;
      
      /*THIS STATIC PART IS IMPORTANT IT LOADS THE IMPORTANT LIBRARIES BEFORE THE PROJECT EXECUTES.*/
      static {
    	    System.loadLibrary("opencv_java3");
    	    System.loadLibrary("OpenCVTest");
    	    
    	}
      
      /*
       *the following is OpenCV code and it manages the opencv libraries and manager.It makes sure whether
       *the Manager is running.If there is no manager then it dowloads it from google play. 
       **/
      private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
          @Override
          public void onManagerConnected(int status) {
              switch (status) {
                  case LoaderCallbackInterface.SUCCESS:
                  {
                      Log.i(TAG, "OpenCV loaded successfully");
                      mOpenCvCameraView.enableView();
                     // System.loadLibrary("OpenCVTest");//load opencv_java lib
                      mOpenCvCameraView.setOnTouchListener( CVTestActivity.this );
                  } break;
                  default:
                  {
                      super.onManagerConnected(status);
                  } break;
              }
          }
      };
  
      /*The activitiy's OnCreate method called when the activity is created.*/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		util = new FileUtils();
		getDisplayMetrics();
		/*Keeps the screen on*/
		getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
		setContentView(R.layout.activity_cvtest);
		mOpenCvCameraView = (CameraBridgeViewBase) findViewById ( R.id.my_camera_view);
        mTextView = (TextView)findViewById(R.id.helpText);
		mOpenCvCameraView.setVisibility( SurfaceView.VISIBLE );
        mOpenCvCameraView.setCvCameraViewListener(this);
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
      
        /*An Alert Messagebox for user info*/
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("Take pictures using this camera.Once you are done,press back and click stitch");
        dlgAlert.setTitle("OpenCVTest");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
        
        /*We get the project name and roomName from previous activity inorder to create folders and images in Secondary
         * storage
         */
        projectName = getIntent().getStringExtra("ProjectName");
        roomName = getIntent().getStringExtra("RoomName");
	}
	
	 public void textClicked(View v) {
		/* Toast.makeText(getApplicationContext(), "Image stitching started", Toast.LENGTH_SHORT).show();
		 int i = MyCameraView.is.stitchImages(MyCameraView.imgs,util.generateCVFilePath());
		 if(i==0) {
			 Toast.makeText(getApplicationContext(), "Image stitched successfully", Toast.LENGTH_LONG).show();
		 }
		 else if(i==-1) {
			 Toast.makeText(getApplicationContext(), "Homographic Estimation failed", Toast.LENGTH_LONG).show();
		 }
		 else if(i==-2) {
			 Toast.makeText(getApplicationContext(), "need More Images", Toast.LENGTH_LONG).show();
		 }
		 else if(i==-3) {
			 Toast.makeText(getApplicationContext(), "camera params failed", Toast.LENGTH_LONG).show();
		 }
		 else {
			 Toast.makeText(getApplicationContext(), "Something went wrong"+i, Toast.LENGTH_LONG).show();
		 }
		 for(int j = 0; j < MyCameraView.imgs.size();j++) {
		 MyCameraView.imgs.remove(j);
		 }*/
		 
	 }
	 
	 @Override
     public void onPause()
     {
         super.onPause();
         if (mOpenCvCameraView != null)
             mOpenCvCameraView.disableView();
         senSensorManager.unregisterListener(this);
     }

     @Override
     public void onResume()
     {
         super.onResume();
         OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this,
                                mLoaderCallback);
         senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

     }

     public void onDestroy() {
         super.onDestroy();
         if (mOpenCvCameraView != null)
             mOpenCvCameraView.disableView();
     }

	@Override
	public void onCameraViewStarted(int width, int height) {
		mRgba = new Mat();
		
	}

	@Override
	public void onCameraViewStopped() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		mRgba = inputFrame.rgba();
       // Imgproc.cvtColor(mRgba, mGray, Imgproc.COLOR_BGRA2GRAY);
		return mRgba;
        
	}

	
	/*This Function is called when the camera is touched.We need to take
	 * Photos when we touch the camera so that code goes here*/
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		MyCameraView cam = (MyCameraView)v;
		
		/*We generate the path for our new file.See FileUtils.java for the definition*/
		String path = util.generateFilePath(projectName,roomName);
		/*Take the picture and store it in the path*/
		cam.takePicture(path);
		distance = 0;
		return false;
	}
	
	public void getDisplayMetrics() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
    	getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    	metrics = displaymetrics;
	}

	/*this function is called every time the sensor is changed*/
	@Override
	public void onSensorChanged(SensorEvent event) {
		 Sensor mySensor = event.sensor;
		 
		    /*make sure that the accelerometer is changed*/
		    if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
		 
		    	float x = event.values[0]; //get X,Y,Z values of the event
		        float y = event.values[1];
		        float z = event.values[2];
		        
		        
		        /*The App polls for sensor change every 100ms This is to increase performance as we dont need 
		         * 100% real time data. This may cause latency but 1/10th of a sec  accuracy is not bad*/
		        long curTime = System.currentTimeMillis();
		        
		        if ((curTime - lastUpdate) > 100) {
		            long diffTime = (curTime - lastUpdate);
		            lastUpdate = curTime;
		            
		           // Toast.makeText(getApplicationContext(), "X =  "+x+" Y = "+y + " Z = "+z, Toast.LENGTH_SHORT).show();
		            float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;
		           
		            
		            /*
		             * As We hold the camera The accelerometer is updated constantly due to the shake in
		             * our hands due to our heartbeat.To avoid this A certain SHAKE_THREHOLD is discarded
		             * as noise. Feel free to play with this value.anything lower than this will not be 
		             * considered as movement. 
		             * */
		            if (speed > SHAKE_THRESHOLD) {
		 
		            	Float speeds = speed; 
		            	Log.i("SPEED",speeds.toString());
		            	//Toast.makeText(getApplicationContext(), "Speed = " + speed,Toast.LENGTH_SHORT).show();
		            	float yAcceleration = Math.abs(y-last_y);
			            /* we are double integerating Velocity in order to get distance.*/
		            	
		            	velocity += yAcceleration*0.1f;
			            distance += velocity*0.1f;
			            Float dis = distance;
			            Log.i("DISTANCE","DISTANCE = "+ dis.toString());
			            Integer vel = (int)velocity;
			            Log.i("DISTANCE","VELOCITY = "+ vel.toString());
			            
			            
		            }
		            if(speed < SHAKE_THRESHOLD) {
		            	velocity = 0;
		            	Float dis = distance;
			            Log.i("DISTANCE","DISTANCE = "+ dis.toString());
			            Integer vel = (int)velocity;
			            Log.i("DISTANCE","VELOCITY = "+ vel.toString());
			                        }
		 
		            last_x = x;
		            last_y = y;
		            last_z = z;
		        }
		    }
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
