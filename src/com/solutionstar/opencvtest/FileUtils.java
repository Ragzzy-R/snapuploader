package com.solutionstar.opencvtest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import android.os.Environment;
/*This is a utility class used for various file meeds */
public class FileUtils {
	
	public static Integer count = 0;
    public Integer count1 = 0;
	public ArrayList<String> files;
	
	
	
	/******************************************************************
	 * 
	 * This function generates a filepath and returns it.As the absolute
	 * path of DCIM in sdcard differs to different devices, to normalize it
	 * we are using this method.
	 * 
	 * 
	 * @param projectPath:The Name of the project
	 * @param roomName: The name of the current room
	 * @return filename
	 */
	public String generateFilePath(String projectPath,String roomName) {
		File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File dir = new File(path.getAbsolutePath()
				+ "/Snapuploader/"+ projectPath+"/"+roomName);
				dir.mkdirs();
				 writeToFile(dir.getAbsolutePath());
		String filename = "IMAGE"+count.toString()+".jpg";
		File file = new File(dir, filename);
		filename = file.toString();
		count++;
		return filename;
	}
	
	/******************************************************************
	 * This is same as generateFilePath but it generates filepath for Zip File
	 * creation
	 * 
	 * 
	 * @param projectPath:The name of the project
	 * @return a filename
	 */
	public static String generateZipPath(String projectPath) {
		File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File dir = new File(path.getAbsolutePath()
				+ "/Snapuploader/");
				dir.mkdirs();
				 
		String filename = projectPath+".zip";
		File file = new File(dir, filename);
		filename = file.toString();
		return filename;
	}
	
	
	/********************************************************************
	 * this is same as generateFilePath but it generates path for OpenCV Pano Images
	 * 
	 * @param projectPath
	 * @param roomName
	 * @return
	 */
	public String generateCVFilePath(String projectPath,String roomName) {
		File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File dir = new File(path.getAbsolutePath()
				+ "/Snapuploader/"+ projectPath+"/"+roomName);
				dir.mkdirs();
				 
		String filename = "PANO"+count1.toString()+".jpg";
		File file = new File(dir, filename);
		filename = file.toString();
		count1++;
		return filename;
	}
	
/*   public  static String[] getFilePaths(String projectPath) {
	   files = new String[count-1];
	   
	   File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File dir = new File(path.getAbsolutePath()
				+ "/Snapuploader/"+ projectPath);
	
	   for(Integer i = 0; i < files.length;i++) {
		   files[i]=dir+"/IMAGE"+i.toString()+".jpg";
		   writeToFile(files[i]);
		   	
	   }
	   return files;
   }*/
	
	
	/***********************************************************************
	 * This function gets the Path of each files to be combined for zipping
	 * 
	 * 
	 * @param projectPath:path of the project
	 * @return
	 */
public ArrayList<String> getFilePaths(String projectPath) {
	   files = new ArrayList<String>();
	   
	   File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File dir = new File(path.getAbsolutePath()
				+ "/Snapuploader/"+ projectPath);
		generateFileList(dir);
	   return files;
   }


	/***********************************************************************
	 * This is a recursive function used to iterate over each file and add
	 * it to the files ArrayList.If the param node is a file then it is added
	 * to the files list if its a directory, then it is recursively called to 
	 * add the list of the files within that directory.
	 * 
	 * @param node
	 */
	public void generateFileList(File node)
	{

	  // add file only
	  if (node.isFile())
	  {
	     files.add((node.toString()));

	  }

	  if (node.isDirectory())
	  {

		  if(node.getName().equalsIgnoreCase("PANO")) {
			  
		  }
		  else {
		//  files.add(node.toString()+"/");
	     String[] subNote = node.list();
	     for (String filename : subNote)
	     {
	        generateFileList(new File(node, filename));
	     }
		 }
	  }
	}
	
	
	/************************************************************
	 * This is a DEBUG function. used to write to some file if logcat cannot 
	 * show any information.Strip it off or disasble it in release build
	 * @param Content
	 */
   public static void  writeToFile(String Content) {
	   BufferedWriter bw =null;
	try {
		bw = new BufferedWriter(new FileWriter(new File("/mnt/sdcard/DCIM/log.txt")));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   try  {
		    bw.write(Content + "\n");
		    bw.close();
		    }catch (FileNotFoundException ex) {
		    System.out.println(ex.toString());
		    } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
   }
	
}
