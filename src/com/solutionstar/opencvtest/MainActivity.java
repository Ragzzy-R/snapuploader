package com.solutionstar.opencvtest;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends ActionBarActivity {

	/*
	 * This is the Mainactivity of the App.This needs lots of work.As the requirement may
	 * change as well as right now it is very bare skeleton.
	 * */
	public String m_Text = "";
	public String roomName = "";
	static ArrayList<String> imgspath = new ArrayList<String>(10);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		showInputPrompt();
	}

	/*****************************************************************
	 * 
	 * We initially show a prompt for user to enter a project name
	 * Once that is done it is used across the app for various 
	 * purposes.If the user didnt give a name the name "Default" is taken
	 * as project name.
	 */
	private void showInputPrompt() {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Welcome!!Please Enter a Project Name");
		builder.setCancelable(false);
		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
		input.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        m_Text = input.getText().toString();
		        m_Text = m_Text.trim();
		    }
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		        m_Text = "Default";
		        m_Text = m_Text.trim();
		    }
		});

		builder.show();
	}

	/*****************************************************
	 *Every time the user clicks take picture a dialogue to ask
	 *for room name is asked.this is used to segregate each room pictures separately
	 */
	private void showRoomPrompt() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Please Enter a Room Name");
		builder.setCancelable(false);
		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
		input.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        roomName = input.getText().toString();
		        roomName = roomName.trim();
		        Intent cameraActivity = new Intent(getBaseContext(),CVTestActivity.class);
				cameraActivity.putExtra("ProjectName", m_Text);
				cameraActivity.putExtra("RoomName", roomName);
				startActivity(cameraActivity);
		    }
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		        roomName = "DefaultRoom";
		        roomName = roomName.trim();
		    }
		});

		builder.show();
	}
	
	/***************************************************
	 * corresponds to the Take Pictures button. see the
	 * layout.xml of this activity for more details.
	 * @param v
	 */
	public void startCamera(View v) {
		
		showRoomPrompt();
	}
	
	/***************************************************
	 * corresponds to the "Stitch Images" button. see the
	 * layout.xml of this activity for more details.
	 * @param v
	 */
	
	public void stitchImages(View v) {
		/*some messages for user*/
        AlertDialog.Builder dlgAlert1  = new AlertDialog.Builder(this);
        dlgAlert1.setMessage("If the notification tray dont show \"Ongoing Image Stitching\" then something terribly went wrong.Try again by restarting the app");
        dlgAlert1.setTitle("OpenCVTest");
        dlgAlert1.setPositiveButton("OK", null);
        dlgAlert1.setCancelable(true);
        dlgAlert1.create().show();
        
        /*some more messasges */
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("Image stitching may take around 10-20 minutes.Check notification tray for update");
        dlgAlert.setTitle("OpenCVTest");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
        
        /*start the image stitching service.*/
		Intent serviceIntent = new Intent(getBaseContext(),StitchService.class);
		 serviceIntent.putExtra("paths",imgspath);
		 serviceIntent.putExtra("ProjectName", m_Text);
		 serviceIntent.putExtra("Roomname", roomName);
		 
		 startService(serviceIntent);
		 
		 for(int i = 0; i <imgspath.size();i++) {
			 imgspath.remove(i);
			 
		 }
	}
	
	/***************************************************
	 * corresponds to the "upload Images" button. see the
	 * layout.xml of this activity for more details.
	 * @param v
	 */
	
	public void uploadImages(View v) {
		
		Intent uploadIntent = new Intent(getBaseContext(),UploadActivity.class);
		FileUtils fileUtils = new FileUtils();;
		uploadIntent.putStringArrayListExtra("imgspath",fileUtils.getFilePaths(m_Text));
		uploadIntent.putExtra("ProjectName", m_Text);
		uploadIntent.putExtra("Roomname", roomName);
		startActivity(uploadIntent);
		 
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
