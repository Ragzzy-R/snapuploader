package com.solutionstar.opencvtest;

import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;



@SuppressWarnings("deprecation")
public class UploadActivity extends ActionBarActivity {

	TextView txtPercentage;
	ProgressBar progressBar;
	String projectName;
	String zipName;
	String[] files;
	HttpURLConnection httpUrlConnection = null;
	ArrayList<String> filePath  = null;
	long totalsize;
	final int BUFFER = 1024;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload);
		txtPercentage = (TextView) findViewById(R.id.txtPercentage);
	//	btnUpload = (Button) findViewById(R.id.btnUpload);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		//imgPreview = (ImageView) findViewById(R.id.imgPreview);
		//vidPreview = (VideoView) findViewById(R.id.videoPreview);
		Intent i = getIntent();
		projectName = i.getStringExtra("ProjectName");
		filePath = i.getStringArrayListExtra("imgspath");
		
		new ZipImages().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.upload, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	
	
	private class ZipImages extends AsyncTask<Void, Integer, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// setting progress bar to zero
			progressBar.setProgress(0);
			txtPercentage.setText("0%");
			Toast.makeText(getApplicationContext(), filePath.size() + " size", Toast.LENGTH_LONG).show();
			
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			// Making progress bar visible
			super.onProgressUpdate(progress);
			progressBar.setVisibility(View.VISIBLE);

			// updating progress bar value
			progressBar.setProgress(progress[0]);

			// updating percentage value
			txtPercentage.setText(String.valueOf(progress[0]) + "%");
			Log.i("Progress", progress[0].toString());
		}

		@Override
		protected String doInBackground(Void... params) {
			files =  new String[filePath.size()];
			for(int j = 0 ; j < filePath.size();j++) {
				
				files[j] = filePath.get(j);
			}
			zipName = FileUtils.generateZipPath(projectName);
			try {
				
				
				BufferedInputStream origin = null;
				FileOutputStream dest = new FileOutputStream(zipName);
				ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
						dest));
				byte data[] = new byte[BUFFER];

				Integer a = files.length;
				Log.i("Compress", a.toString());
				for (int i = 0; i < files.length; i++) {
					Log.v("Compress", "Adding: " + files[i]);
					FileInputStream fi = new FileInputStream(files[i]);
					origin = new BufferedInputStream(fi, BUFFER);

					String[] file = files[i].split(projectName+"/");
					
					ZipEntry entry = new ZipEntry(files[i].substring(files[i].lastIndexOf(projectName+"/")));
					//ZipEntry entry = new ZipEntry(file[i]);
					FileUtils.writeToFile(file[1]);
					out.putNextEntry(entry);
					float length = files.length;
					Float f = (i/length)*100;
					float f1 = f;
					Log.i("PROGRESS", f.toString());
					publishProgress((int)f1);
					int count;

					while ((count = origin.read(data, 0, BUFFER)) != -1) {
						out.write(data, 0, count);
					}
					origin.close();
				}

				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			
			return zipName;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.e("Server","Response from server: " + result);
			txtPercentage.setText("100%");
			progressBar.setProgress(100);
			// showing the server response in an alert dialog
			showAlert("Successfully Zipped files at:  "+ result);

			
		}

	}
	
	private void showAlert(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setTitle("Zipped Successfully")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	} 
		 
	public void zip(String[] _files, String zipFileName) {
			}
}

