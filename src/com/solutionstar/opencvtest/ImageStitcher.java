package com.solutionstar.opencvtest;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;

import android.util.Log;
import android.widget.Toast;

public class ImageStitcher {

	/*public int stitchImages(ArrayList<Mat>img,String path) {
		int elems=  img.size();
        //Log.v("Matobjdata", "from native = " + ans);
        long[] tempobjadr = new long[elems]; 
        for (int i=0;i<elems;i++)
        {
            Mat tempaddr=img.get(i);
            tempobjadr[i]=  tempaddr.getNativeObjAddr();
        }

        //call to native function
        Integer i = nativeStitchImages(path,tempobjadr);
        Log.i("RET", i.toString());
        return i;
	}*/
	
	/**************************************************************
	 * 
	 * Although the function takes an ArrayList<String> as param
	 * it is wiser to convert it into a String[] since we are 
	 * calling a Native method.ArrayList needs lots of manipulations
	 * when moving them from java to C++. As C++'s  ArrayList is different
	 * from java's.
	 * 
	 * @param imgpaths: the paths of each images to be stitched
	 * @param path:The path of the resulting Pano image
	 * @returns an integer which is used for error detection.
	 */
	public int stitchImages(ArrayList<String>imgpaths,String path) {
		int elems=  imgpaths.size();
        //Log.v("Matobjdata", "from native = " + ans);
        String[] temp = new String[elems]; 
        for (int i=0;i<elems;i++)
        {
            temp[i]=imgpaths.get(i);
            
        }

        //call to native function
        Integer i = nativeStitchImages(path,temp); /*calling C++ function*/
        Log.i("RET", i.toString());
        return i;
	}
	public int addMatToVector(Mat mat) {
		return (nativeAddMatToVector(mat.getNativeObjAddr()));
	}
	
	
	/*The application uses native  openCV code in C++ using JNI calls. so thees methods are 
	 * declared here their defenitions are given in C++ files.
	 * */
	public native int nativeStitchImages(String path,String[] img);
	public native int nativeAddMatToVector(long matAddr);
}
