package com.solutionstar.opencvtest;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.widget.Toast;

public class StitchService extends Service {

	String projectName;
	String roomName;
	public String panoPathName; 
	static {
	    System.loadLibrary("opencv_java3");
	    System.loadLibrary("OpenCVTest");
	    
	}
	ImageStitcher is = new ImageStitcher();
	public Integer STITCH_STATUS = -10;
	 FileUtils util = new FileUtils();
	 ArrayList<String> list;
	 Integer count = 0;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	   public int onStartCommand(Intent intent, int flags, int startId) {
	      // Let it continue running until it is stopped.
	    	Toast.makeText(getApplicationContext(), "Image sitcher Service Started", Toast.LENGTH_LONG).show();
	     //notifyUser("HELLO","HELLO");	
	    	list = (ArrayList<String>) intent.getSerializableExtra("paths");
	    	projectName = intent.getStringExtra("ProjectName");
	    	roomName = intent.getStringExtra("roomName");
	    	panoPathName = util.generateCVFilePath(projectName,"PANO");
	    	Toast.makeText(getApplicationContext(), panoPathName, Toast.LENGTH_LONG).show();
	    	startOnForeground();
	    	new StitchImage().execute(is);
	    	viewHandler.post(updateView);
	      return START_NOT_STICKY;
	   }
	
	@SuppressWarnings("deprecation")
	private void startOnForeground() {
		
		/*Notification notification = new Notification(R.drawable.ic_launcher, (CharSequence)"Image Stitching started",
		        System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, CVTestActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(this, "OpenCV Test",
		        "ONGOING IMAGE STITCHING-please dont close the app from recent apps", pendingIntent);
		startForeground(999, notification);*/
		
		
		 final NotificationCompat.Builder builder = new Builder(this);
	        builder.setSmallIcon(R.drawable.stitch_pressed);
	        builder.setContentTitle("OpenCV Test");
	        builder.setTicker("ticker");
	        builder.setContentText("ONGOING IMAGE STITCHING");
	        Intent notificationIntent = new Intent(this, CVTestActivity.class);
			final PendingIntent pi = PendingIntent.getActivity(this, 0, notificationIntent, 0);
	        builder.setContentIntent(pi);
	        final Notification notification = builder.build();
	        // notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
	        // notification.flags |= Notification.FLAG_NO_CLEAR;
	        // notification.flags |= Notification.FLAG_ONGOING_EVENT;

	        startForeground(999, notification);
	        // mNotificationManager.notify(NOTIFICATION_ID, notification);
		
	}

	Handler viewHandler = new Handler();
    Runnable updateView = new Runnable(){
      @Override
      public void run(){
         if(STITCH_STATUS!=-10) {
         stopSelf();
         }
         viewHandler.postDelayed(updateView, 1000);
      }
    };
	   
	@Override
	   public void onCreate() {
		super.onCreate();
		Toast.makeText(getApplicationContext(), "Image sitcher Oncreate called", Toast.LENGTH_LONG).show();   
	}
	   @Override
	   public void onDestroy() {
	      super.onDestroy();
	      Toast.makeText(this, "Image Stitching finished", Toast.LENGTH_LONG).show();
	      if(STITCH_STATUS==0) {
			 notifyUser("OpenCV Test","Image stitched successfully");
		 }
		 else if(STITCH_STATUS==-1) {
			 notifyUser("OpenCV Test","Homographic Estimation Failed, Try Again!");
		 }
		 else if(STITCH_STATUS==-2) {
			 notifyUser("OpenCV Test","Stitching Failed-Need More images");
		 }
		 else if(STITCH_STATUS==-3) {
			 notifyUser("OpenCV Test","Camera params failed");
		 }
		 else {
			 notifyUser("OpenCV Test","Something Went wrong-Unkown error"+ STITCH_STATUS.toString());
		 }
	      
	      stopForeground(true);
		
	   }
	   
	   private void notifyUser(String notificationTitle, String notificationMessage){
		     
		   NotificationCompat.Builder mBuilder =
				    new NotificationCompat.Builder(this)
				    .setSmallIcon(R.drawable.stitch_enabled)
				    .setContentTitle(notificationTitle)
				    .setContentText(notificationMessage);
		  
		   mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

		     //LED
		        mBuilder.setLights(Color.RED, 3000, 3000);
		        
		        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		        mBuilder.setSound(uri);
		   NotificationManager mNotifyMgr = 
			        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			// Builds the notification and issues it.
		   Intent notificationIntent;
		   
		   if(STITCH_STATUS!=0) {
			   notificationIntent = new Intent(getBaseContext(), PanoActivity.class);
			   
		   }
		   else {
			   notificationIntent = new Intent(getBaseContext(), PanoActivity.class);
			   
		   }
		   notificationIntent.putExtra("path", panoPathName);
		    FileUtils.writeToFile(panoPathName+"BCD");
		    Random r = new Random();
		    r.setSeed(10000000);
		    int RANDOM_REQUEST_CODE = r.nextInt(1000);
		   final PendingIntent pi = PendingIntent.getActivity(this, RANDOM_REQUEST_CODE, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	        mBuilder.setContentIntent(pi);
	        int id = r.nextInt(1000);
	        mNotifyMgr.notify(id, mBuilder.build());
		   
		   }
	   
	   private class StitchImage extends AsyncTask<ImageStitcher,Integer,Integer> {

		@Override
		protected Integer doInBackground(ImageStitcher... params) {
			
			return params[0].stitchImages(list, panoPathName);
			
		}
		
		@Override
		protected void onPostExecute(Integer result) {
		
			StitchService.this.STITCH_STATUS = result;
			Toast.makeText(getApplicationContext(), "RESULT VALUE = " + result.toString(), Toast.LENGTH_LONG).show();
			
		}
		   
	   }
}
