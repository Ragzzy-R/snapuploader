package com.solutionstar.opencvtest;

import java.io.*;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

public class PanoActivity extends Activity {

	ImageView images;
	String path;
	String TAG = "PANO";
	Bundle extras;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pano);
		images = (ImageView)findViewById(R.id.imageView3);
		//extras = getIntent().getExtras();
		path  = getIntent().getStringExtra("path");
		
		//path = getIntent().getStringExtra("path");
		//path = "/mnt/sdcard/DCIM/Snapuploader/home/PANO/PANO0.jpg";
		//FileUtils.writeToFile(path+"ABC");
		Bitmap map = decodeSampledBitmapFromResource(path,300,300);
		Toast.makeText(getApplicationContext(), path+"AAA", Toast.LENGTH_LONG).show();
		//FileUtils.writeToFile(map.toString());
		if(map!=null) {
		images.setImageBitmap(map);
		//images.setImageBitmap(map);
		}
	}
	
	
	
	public  int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    return inSampleSize;
}
	
	
	public  Bitmap decodeSampledBitmapFromResource(String path,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile (path, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeFile(path, options);
	}
}
