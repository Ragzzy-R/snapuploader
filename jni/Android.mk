LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include /home/cuic/OpenCV-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_LDLIBS += -llog
LOCAL_MODULE    := OpenCVTest
LOCAL_SRC_FILES := OpenCVTest.cpp

LOCAL_STATIC_LIBRARIES := opencv_core
LOCAL_STATIC_LIBRARIES += opencv_java3
#LOCAL_STATIC_LIBRARIES += opencv_stitching

#LOCAL_C_INCLUDES := /usr/include/c++/5.2.0/
#LOCAL_C_INLCUDES += /usr/include/c++/5.2.0/x86_64-unknown-linux-gnu/
LOCAL_C_INCLUDES := /home/cuic/OpenCV-android-sdk/sdk/native/jni/include/
#LOCAL_ALLOW_UNDEFINED_SYMBOLS := true



include $(BUILD_SHARED_LIBRARY)
