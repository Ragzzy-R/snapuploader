#include <jni.h>
#include <iostream>
#include <fstream>
#include <android/log.h>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/stitching.hpp"
#include <vector>
#include <string>
using namespace std;
using namespace cv;
bool try_use_gpu = false;
string result_name = "result.jpg";

vector<Mat> readImages(vector<string> paths);
vector<string> parseArrayElements(JNIEnv *env,jobjectArray imgspath);
const char* parseOutputPath(JNIEnv *env,jstring path);
jmethodID ArrayList_get_id;
jvalue arg;
vector<Mat> imgs2;
extern "C" {
/*JNIEXPORT jint JNICALL Java_com_solutionstar_opencvtest_ImageStitcher_nativeStitchImages
  (JNIEnv *env , jobject obj, jstring path, jlongArray mataddrs) {
    //int retval = parseCmdArgs(argc, argv);
    //if (retval) return -1;

	const char *str= (*env).GetStringUTFChars(path,0);
		//need to release this string when done with it in order to
		//avoid memory leak
	ofstream outputFile;
			outputFile.open("/mnt/sdcard/DCIM/abc.txt");
			outputFile << "RAGHURAM IS A GOOD BOY" << endl;


		vector<Mat> imgs1;
		jsize a_len = (*env).GetArrayLength(mataddrs);
		jlong *traindata = (*env).GetLongArrayElements(mataddrs,0);
		for(int k=0;k<a_len;k++)
		{
		    Mat & newimage=*(Mat*)traindata[k];
		    imgs1.push_back(newimage);
		}
		// do the required manipulation on the images;
		(*env).ReleaseLongArrayElements(mataddrs,traindata,0);

	Mat pano;
	imgs2.push_back(imread("/mnt/sdcard/DCIM/KualaLumpur001.JPG"));
	imgs2.push_back(imread("/mnt/sdcard/DCIM/KualaLumpur002.JPG"));
	imgs2.push_back(imread("/mnt/sdcard/DCIM/KualaLumpur003.JPG"));
	imgs2.push_back(imread("/mnt/sdcard/DCIM/KualaLumpur004.JPG"));
	imgs2.push_back(imread("/mnt/sdcard/DCIM/KualaLumpur005.JPG"));

	//imgs2.push_back(imread("/mnt/sdcard/DCIM/IMAGE5.jpg"));

    Stitcher stitcher = Stitcher::createDefault(try_use_gpu);
    Stitcher::Status status = stitcher.stitch(imgs1, pano);

   if (status == Stitcher::ERR_HOMOGRAPHY_EST_FAIL)
    {
        cout << "Can't stitch images, error code = " << int(status) << endl;
        return -1;
    }
    if(status == Stitcher::ERR_NEED_MORE_IMGS) {
    	cout<< "need more images";
    	return -2;
    }
    if(status == Stitcher::ERR_CAMERA_PARAMS_ADJUST_FAIL) {
    	cout<<"params adjust fail";
    	return -3;
    }

    if(status == Stitcher::OK) {
    imwrite(str, pano);
    (*env).ReleaseStringUTFChars(path, str);

    return 0;
    }
    return -5;
}*/



JNIEXPORT jint JNICALL Java_com_solutionstar_opencvtest_ImageStitcher_nativeStitchImages
  (JNIEnv *env , jobject obj, jstring path, jobjectArray imgspath) {

	const char* str = parseOutputPath(env,path);
	vector<string> paths = parseArrayElements(env,imgspath);
	vector<Mat> imgs1 = readImages(paths);
	Mat pano;

		//imgs2.push_back(imread("/mnt/sdcard/DCIM/IMAGE5.jpg"));

	    Stitcher stitcher = Stitcher::createDefault(try_use_gpu);
	    Stitcher::Status status = stitcher.stitch(imgs1, pano);

	   if (status == Stitcher::ERR_HOMOGRAPHY_EST_FAIL)
	    {
	        cout << "Can't stitch images, error code = " << int(status) << endl;
	        return -1;
	    }
	    if(status == Stitcher::ERR_NEED_MORE_IMGS) {
	    	cout<< "need more images";
	    	return -2;
	    }
	    if(status == Stitcher::ERR_CAMERA_PARAMS_ADJUST_FAIL) {
	    	cout<<"params adjust fail";
	    	return -3;
	    }

	    if(status == Stitcher::OK) {
	    imwrite(str, pano);
	    (*env).ReleaseStringUTFChars(path, str);

	    return 0;
	    }
	    return -5;


}

}

vector<Mat> readImages(vector<string> paths) {

	vector<Mat> imgs;
	for (std::vector<string>::iterator it = paths.begin() ; it != paths.end(); ++it) {
		ofstream outputFile;
					outputFile.open("/mnt/sdcard/DCIM/cde.txt",std::ios::app);
					outputFile << *(it)<< endl;

					Mat m = imread(*it);
					Mat n;
					int rows = m.rows;
					int cols = m.cols;

					Size s = m.size();
					Size s1;
					s1.height = s.height/2;
					s1.width = s.width/2;
					outputFile<<"Height = "<<s1.height<<"Width = "<<s1.width<<endl;
					resize(m,n,s1);
		imgs.push_back(n);
	}
	return imgs;
}


vector<string> parseArrayElements(JNIEnv *env,jobjectArray imgspath) {
	vector<string> imgs1;
			jsize a_len = (*env).GetArrayLength(imgspath);

			for(int k=0;k<a_len;k++)
			{
				jstring path = (jstring)(*env).GetObjectArrayElement(imgspath,k);
				const char* temp= (*env).GetStringUTFChars(path,0);
				string newpath = temp;
			    imgs1.push_back(newpath);
			    (*env).ReleaseStringUTFChars(path,temp);
			}
			// do the required manipulation on the images;
	return imgs1;
}

const char* parseOutputPath(JNIEnv *env,jstring path) {
const char *str= (*env).GetStringUTFChars(path,0);
		//need to release this string when done with it in order to
		//avoid memory leak
	ofstream outputFile;
			outputFile.open("/mnt/sdcard/DCIM/abc.txt");
			outputFile << "RAGHURAM IS A GOOD BOY" << endl;
			return str;
}

